import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

class Node{
	String name;
	long suara1;
	long suara2;
	Node parent;
	int index1;
	int index2;
	
	public Node(String name) {
        this.name = name;
        this.suara1 = 0;
        this.suara2 = 0;
        this.parent = null;
        this.index1 = 0;
        this.index2 = 0;
    }
	
//	void addSuara1(long suara1, Set<String> menang1, Set<String> menang2, Integer[] persenan1, Integer[] persenan2) {
//		this.suara1+=suara1;
////		System.out.println(name + " " + suara1 + " " + suara2);
//		menang(menang1, menang2);
////		System.out.println("hore");
//		if (this.parent != null) {
//			this.parent.addSuara1(suara1, menang1, menang2, persenan1, persenan2);
//		}
//	}
//	
	long getSuara1(){
		return this.suara1;
	}
	
	void addSuara(long suara1, long suara2, Set<String> menang1, Set<String> menang2, Integer[] persenan1, Integer[] persenan2) {
		if (persenan1[this.index1] > 0) {
			persenan1[this.index1] -= 1;
		}
		if (persenan2[this.index2] > 0) {
			persenan2[this.index2] -= 1;
		}
		this.suara1+=suara1;
		this.suara2+=suara2;
//		System.out.println(name + " " + suara1 + " " + suara2);
		menang(menang1, menang2);
		
		this.index1 = addPersen(1);
		this.index2 = addPersen(2);
		//System.out.println(this.name + " " + index1 + " " + index2);
		persenan1[this.index1] +=1;
		persenan2[this.index2] +=1;
		if (this.parent != null) {
			this.parent.addSuara(suara1, suara2, menang1, menang2, persenan1, persenan2);
		}
	}
	
	long getSuara2(){
		return this.suara2;
	}
	
	void addParent(Node parent) {
		this.parent=parent;
	}  
	
	Node getParent(){
		return this.parent;
	}
	
	void menang(Set<String> menang1, Set<String> menang2) {
		if (this.suara1 < this.suara2) {
			menang2.add(name);
			if (menang1.contains(name)) {
				menang1.remove(name);
			}
		}
		else if (this.suara1 > this.suara2) {
			menang1.add(name);
			if (menang2.contains(name)) {
				menang2.remove(name);
			}
		}
		else if (this.suara1 == this.suara2) {
			if (menang1.contains(name)) {
				menang1.remove(name);
			}
			if (menang2.contains(name)) {
				menang2.remove(name);
			}
		}
	}
	int addPersen(int param) {
		int one = 0;
		int two = 0;
		if (this.suara1 == this.suara2 ) {
			one = 50;
			two = 50;
		}
		else if ((this.suara1 + this.suara2) != 0) {
			one = (int)((this.suara1*100/(this.suara1+this.suara2)));
			two = (int)((this.suara2*100/(this.suara1+this.suara2)));
		}
		int nilai = (param == 1) ? one : two;
		return nilai;
			
	}
}

public class PemiluDonatRaya {
    private static HashMap<String, Node> listWilayah = new HashMap<String, Node>();
    private static ArrayList<String> key = new ArrayList<String>();
    private static Set<String> menang1 = new HashSet<String>(); 
    private static Set<String> menang2 = new HashSet<String>();
    private static Integer[] persenan1 = new Integer[101];
    private static Integer[] persenan2 = new Integer[101];
    
    public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		
		for (int i= 0; i <persenan1.length; i++) {
			persenan1[i] = 0;
			persenan2[i] = 0;
		}
		String[] peserta = br.readLine().split(" ");
		int provinsi = Integer.parseInt(br.readLine());
		for (int i = 0; i < provinsi; i++) {
			String[] inputWilayah = br.readLine().split(" ");
			Node a = new Node(inputWilayah[0]);
			a.addSuara(0, 0, menang1, menang2, persenan1, persenan2);
			if (listWilayah.containsKey(inputWilayah[0])) {
				a.addParent(listWilayah.get(inputWilayah[0]));
			} else {
				listWilayah.put(inputWilayah[0], a);
				key.add(inputWilayah[0]);
			}
			int anak = Integer.parseInt(inputWilayah[1]);
			for (int j = 0; j < anak; j++) {
				Node b = new Node(inputWilayah[j+2]);
				a.addSuara(0, 0, menang1, menang2, persenan1, persenan2);
				b.addParent(a);
				listWilayah.put(inputWilayah[j+2], b);
				key.add(inputWilayah[j+2]);
			}
			
        }    
//		for (String keys : listWilayah.keySet()) {
//		   System.out.println(keys);}
		int operasi = Integer.parseInt(br.readLine());
		for (int o = 0; o < operasi; o++) {
			String[] inputOperasi = br.readLine().split(" ");
			if (inputOperasi[0].equals("TAMBAH")) {
				Node wilayah = listWilayah.get(inputOperasi[1]);
//				System.out.println(wilayah.getParent());
				wilayah.addSuara(Long.parseLong(inputOperasi[2]),Long.parseLong(inputOperasi[3]), menang1, menang2, persenan1, persenan2);
//				wilayah.addSuara2(Long.parseLong(inputOperasi[3]), menang1, menang2, persenan1, persenan2);
				
			}
			else if (inputOperasi[0].equals("ANULIR")) {
				Node wilayah = listWilayah.get(inputOperasi[1]);
				wilayah.addSuara(-Long.parseLong(inputOperasi[2]),-Long.parseLong(inputOperasi[3]), menang1, menang2, persenan1, persenan2);
			}
			else if (inputOperasi[0].equals("CEK_SUARA")) {
				Node wilayah = listWilayah.get(inputOperasi[1]);
				System.out.println(wilayah.getSuara1()+ " " + wilayah.getSuara2());
			}
			else if (inputOperasi[0].equals("WILAYAH_MENANG")) {
				if (inputOperasi[1].equals(peserta[0])) {
					System.out.println(menang1.size());
				}
				else {
					System.out.println(menang2.size());
				}
			}
			else if (inputOperasi[0].equals("WILAYAH_MINIMAL")) {
				int persen = Integer.parseInt(inputOperasi[2]);
				if (inputOperasi[1].equals(peserta[0])) {
					for (int i = 0; i <persenan1.length; i++) {
//						System.out.print(persenan1[i] +":" + i + "//");
					}
					int hasilPersen = 0;
					for (int i = persen; i < persenan1.length; i++) {
						hasilPersen+=persenan1[i];
					}
					System.out.println(hasilPersen);
				}
				else {
					for (int i = 0; i <persenan2.length; i++) {
//						System.out.print(persenan2[i] +":" + i + "//");
					}
					int hasilPersen = 0;
					for (int i = persen; i < persenan2.length; i++) {
						hasilPersen+=persenan2[i];
					}
					System.out.println(hasilPersen);
				}
			}
			else if (inputOperasi[0].equals("WILAYAH_SELISIH")) {
				long count = 0;
				long selisih = Long.parseLong(inputOperasi[1]);
				for (String ky : key) {
					long donat1 = listWilayah.get(ky).getSuara1();
					long donat2 = listWilayah.get(ky).getSuara2();
					if (Math.abs(donat1-donat2) >= selisih) {
						count++;
					}
				}
				System.out.println(count);
			}
			else if (inputOperasi[0].equals("CEK_SUARA_PROVINSI")) {
				Node parent = listWilayah.get(key.get(0));
				for (int i = 1; i < key.size() ; i++) {
					Node prov = listWilayah.get(key.get(i));
					if (prov.getParent() != parent) {
						break;
					}
					else {
						System.out.println(prov.name + " " + prov.getSuara1() + " " + prov.getSuara2());
					}
				}
			}
		}
			
    }
    
/*    public static long searchMenang(int param) {
    	long count1 = 0;
    	long count2  = 0;
    	for (String ky : key) {
//    		System.out.println(ky);
    		long satu = listWilayah.get(ky).getSuara1();
    		long dua = listWilayah.get(ky).getSuara2();
    		if (satu > dua) {
//    			System.out.println("ehe");j
    			count1 += 1;
    		}
    		else if (dua > satu) {
//    			System.out.println("oho");
    			count2 += 1;
    		}
    	}
    	long menang = (param == 1) ? count1 : count2;
    	return menang;
    }*/
    
    public static long searchMinim(int peserta, float persen) {
    	long count = 0;
    	for (String ky : key ) {
//   		System.out.println(ky + " " +listWilayah.get(ky).getSuara1() + " " + listWilayah.get(ky).getSuara2());
    		long satu = listWilayah.get(ky).getSuara1();
    		long dua = listWilayah.get(ky).getSuara2();
			long one = 0;
			long two = 0;
			if (satu == 0 && dua == 0) {
				one = 50;
				two = 50;
			}
			if ((satu + dua) != 0) {
				one = ((satu*100/(satu+dua)));
				two = ((dua*100/(satu+dua)));
			}
			if (peserta == 1) {
//    					System.out.println("yeay");
				if ( one >= persen) {
//    						System.out.println("satu");
					count+=1;
				}
			}
			else if (peserta == 2){
//				System.out.println("yuhu");
				if (two >= persen) {
//					System.out.println("dua" + " " + two);
					count+=1;
				}
			}	
    	}
    	return count;
    }
    
}
